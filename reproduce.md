# Reproduce

## Data preparation

```Bash
cd research/im2txt/data/mscoco/images

wget http://images.cocodataset.org/zips/train2014.zip
wget http://images.cocodataset.org/zips/val2014.zip
wget http://images.cocodataset.org/zips/val2014.zip
```

unzip all of them

```Bash
cd research/im2txt/data/mscoco

wget http://images.cocodataset.org/annotations/annotations_trainval2014.zip
unzip annotations_trainval2014.zip
```


## Environment

Python2 needed

```Bash
sudo apt install python-tk
pip install Cython
pip install pdfminer==20140328
apt install libmysqlclient-dev
pip install -r requirements.txt
```

```Python
import nltk
nltk.download('punkt')
```

## Data preprocessing

```Bash
cd women-snowboard/research/im2txt/scripts
python SegmentationMasks.py

cd women-snowboard/research/im2txt

python im2txt/data/build_scripts/build_mscoco_data.py \
 --train_image_dir=data/mscoco/images/train2014 \
 --val_image_dir=data/mscoco/images/val2014 \
 --train_captions_file=data/mscoco/annotations/captions_train2014.json \
 --val_captions_file=data/mscoco/annotations/captions_val2014.json \
 --output_dir=im2txt/data/mscoco_base \
 --word_counts_output_file="data/word_counts.txt" 

python im2txt/data/build_scripts/build_mscoco_blocked_and_biased.py \
 --train_image_dir=data/mscoco/images/train2014 \
 --val_image_dir=data/mscoco/images/val2014 \
 --train_captions_file=data/mscoco/annotations/captions_train2014.json \
 --val_captions_file=data/mscoco/annotations/captions_val2014.json \
 --output_dir=im2txt/data/bias_and_blocked \
 --word_counts_output_file="data/word_counts.txt" \
 --blocked_dir="data/blocked_images_average/"

 python im2txt/data/build_scripts/build_mscoco_single_gender_blocked.py \
 --train_image_dir=data/mscoco/images/train2014 \
 --val_image_dir=data/mscoco/images/val2014 \
 --train_captions_file=data/mscoco/annotations/captions_train2014.json \
 --val_captions_file=data/mscoco/annotations/captions_val2014.json \
 --output_dir=im2txt/data/man \
 --word_counts_output_file="data/word_counts.txt" \
 --blocked_dir="data/blocked_images_average/" \
 --gender="man"

 python im2txt/data/build_scripts/build_mscoco_single_gender_blocked.py \
 --train_image_dir=data/mscoco/images/train2014 \
 --val_image_dir=data/mscoco/images/val2014 \
 --train_captions_file=data/mscoco/annotations/captions_train2014.json \
 --val_captions_file=data/mscoco/annotations/captions_val2014.json \
 --output_dir=im2txt/data/woman \
 --word_counts_output_file="data/word_counts.txt" \
 --blocked_dir="data/blocked_images_average/" \
 --gender="woman"
```

## Training

research/im2txt/train_scripts/train_baseline_ft.sh

```Bash
BLOCKED_MSCOCO_DIR="im2txt/data/bias_and_blocked"
INCEPTION_CHECKPOINT="final_weights_eccv2018/inception_checkpoint/inception_v3.ckpt"
MODEL_DIR="<SOME_PATH>/women-snowboard/research/im2txt/trained_models"

python im2txt/train.py \
#  --init_from="${INIT_MODEL_DIR}/train" \
  --input_file_pattern="${BLOCKED_MSCOCO_DIR}/train-?????-of-00256" \
  --inception_checkpoint_file="${INCEPTION_CHECKPOINT}" \
  --train_dir="${MODEL_DIR}/baseline/train" \
  --train_inception=True \
  --number_of_steps=1500000 \
  --batch_size=8
```

"init_from" should be commented out
Other commands are similar

